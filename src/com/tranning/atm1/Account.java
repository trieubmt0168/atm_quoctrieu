package com.tranning.atm1;

public class Account {
    int accountNo;
    String password;
    double amount;
    String customerName;
    boolean role;

    public Account() {
        accountNo = 0;
        password = "";
        amount = 0;
        customerName = "";
        role = true;

    }

    public Account(int accountNo, String password, double amount, String customerName, boolean role) {
        this.accountNo = accountNo;
        this.password = password;
        this.amount = amount;
        this.customerName = customerName;
        this.role = role;
    }

    public int getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(int accountNo) {
        this.accountNo = accountNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Boolean getRole() {
        return role;
    }

    public void setRole(boolean role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNo=" + accountNo +
                ", password='" + password + '\'' +
                ", amount=" + amount +
                ", customerName='" + customerName + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}
