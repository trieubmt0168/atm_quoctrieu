package com.tranning.atm1;

import com.tranning.atm1.Account;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.tranning.atm1.ATMService.*;

public class AtmMain {
    static Account account;



    public static void main(String[] args) throws IOException {

        System.out.println("Welcome to ATM Application");
        Scanner sc = new Scanner(System.in);

        List<Account> accountList = new ArrayList<>();
        // Tạo một tài khoản mới và gán giá trị cho nó
        Account acc1 = newAccount(1, "123456", 100, "Nguyen Van A");
        Account acc2 = newAccount(2, "123456", 50, "Tran Van B");
        accountList.add(acc1);
        accountList.add(acc2);


        System.out.println(account);

        String choice = "y";
        while (choice.equalsIgnoreCase("y")) {
            // Khoi tao menu
            System.out.println("***********************************");
            System.out.println("Select your action: ");
            System.out.println("| 1-Đăng nhập                     |");
            System.out.println("| 2-Xem thông tin tài khoản       |");
            System.out.println("| 3-Thêm tài khoản                |");
            System.out.println("| 4- Kích hoạt tài khoản          |");
            System.out.println("| 5-Thêm tiền vào cây             |");
            System.out.println("| 6-Chuyển tiền                   |");
            System.out.println("***********************************");
            int action = sc.nextInt();
            switch (action) {
                case 1:
                    //Goi ham login
                    if (login(account)) {
                        System.out.println("Đang nhập thành công");
                    } else
                        System.out.println("Đăng nhạp thất bại");
                    break;
                case 2:
                    viewAccount(accountList);

                    break;

                case 3:
                    // tạo tài khoản
                    account = createAccount();
                    accountList.add(account);
                    break;

                case 4:
                    // Goi hàm kích hoạt
                     EnableAccount(accountList);
//                    if (recharge(account)) {
//                        System.out.print("Nạp thành công");
//                    } else
//                        System.out.print("nạp thất bại");
                    break;
                case 5:
                    moneynumber();
                    break;
                case 6:

                    // Goi thu tuc chuyển tiền
                    if (transfer(acc1, acc2)) {
                        System.out.println("Chuyển tiền thành công");
                    } else
                        System.out.println("Chuyển tiền thất bại");
                    break;
                default:
                    System.out.println("Không hợp lệ");
                    break;
            }//End switch... case
            System.out.println("Tiếp tục hay không? (Y/N)");
            choice = sc.next();
            System.out.println();

        }
    }


}
