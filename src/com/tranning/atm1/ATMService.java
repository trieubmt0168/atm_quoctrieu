package com.tranning.atm1;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ATMService {
    private static int Five_hundred_throusand = 500000;
    private static final int Two_hundred_throusand = 200000;
    private static final int one_hundred_throusand = 100000;
    private static final int Fifty_throusand = 50000;
    static final int Tweenty_throusand = 20000;
    static final int Ten_throusand = 10000;

    private static int fivehundredthrousand = 0;
    static int twohundredthrousand = 0;
    static int onehundredthrousand = 0;
    static int tweentythrousand = 0;
    static int fiftythrousand = 0;
    static int tenthrousand = 0;

    // Ham chuyen tien
    public static boolean transfer(Account acc1, Account acc2) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhập số tiền muốn chuyển:");
        double amount = sc.nextDouble();
        if (acc1.getAmount() >= amount) {
            acc1.setAmount(acc1.getAmount() - amount);
            acc2.setAmount(acc2.getAmount() + amount);
            return true;
        } else
            return false;
    }


    // Ham nap tien
    public static boolean recharge(Account acc) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Số tiền bạn muốn nạp:");
        double amount = sc.nextDouble();
        if (amount >= 0) {
            acc.setAmount(acc.getAmount() + amount);
            return true;
        } else
            return false;
    }

    // Ham rut tien
    public static boolean withdraw(Account acc) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Số tiền bạn muốn rút:");
        double amount = sc.nextDouble();
        if (amount <= acc.getAmount()) {
            acc.setAmount(acc.getAmount() - amount);
            return true;
        } else
            return false;
    }

    // Ham xu ly login
    public static boolean login(Account acc) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhập số tài khoản:");
        int accNo = sc.nextInt();
        System.out.print("nhập mật khẩu:");
        String pass = sc.next();
        return acc.getAccountNo() == accNo && acc.getPassword().equals(pass);
    }

    public static Account createAccount() {
        Account account = new Account();
        Scanner sc = new Scanner(System.in);
//        tạo tài khoản
        System.out.print("Nhập số tài khoản mới :");
        int accNo = sc.nextInt();
        if (accNo >= 3) {
            account.setAccountNo(accNo);
        }
        System.out.print("Nhập mật khẩu :");
        String passa = sc.next();
        if (passa != null) {
            account.setPassword(passa);
        }
        System.out.print("Nhập số tiền :");
        int amount = sc.nextInt();
        if (amount >= 0) {
            account.setAmount(amount);
        }
        System.out.print("Nhập tên kh :");
        String CustomerName = sc.next();
        if (CustomerName != null) {
            account.setCustomerName(CustomerName);
        }
        return account;
    }

    public static Account newAccount(int accNo, String pass, double amount, String custName) {
        Account acc = new Account();
        acc.setAccountNo(accNo);
        acc.setPassword(pass);
        acc.setAmount(amount);
        acc.setCustomerName(custName);
        return acc;
    }

    // Danh sách account:
    public static void viewAccount(List<Account> acc) {
        for (Account account : acc) {
            System.out.println("stk: " + account.getAccountNo()
                    + ", pass : " + account.getPassword() + ", amount :" + account.getAmount() + ", custommer :" + account.getCustomerName());
        }
    }

    public static Account EnableAccount(List<Account> accounts) {

        Account acc = new Account();
        for (Account account : accounts) {
            System.out.println("stk: " + account.getAccountNo()
                    + ", pass : " + account.getPassword() + ", amount :" + account.getAmount()
                    + ", custommer :" + account.getCustomerName());
            if (acc != null) {
                acc.setRole(true);
            }
        }
        return acc;
    }

    public static void  moneynumber() {
        DecimalFormat decimalFormat = new DecimalFormat("#,###");
        int money = 0;
        Scanner sc = new Scanner(System.in);

        do {
            System.out.println("Your money");
            money = sc.nextInt();
        } while (money % 10000 != 0);
//        sc.close();
      if(money >= Five_hundred_throusand){
          fivehundredthrousand = money / Five_hundred_throusand;
          money = money % Five_hundred_throusand;

          System.out.printf("Mệnh giá %s: có %d tờ %n", decimalFormat.format(Five_hundred_throusand), fivehundredthrousand);
      }

        if(money >= Two_hundred_throusand){
            twohundredthrousand = money / Two_hundred_throusand;
            money = money % Two_hundred_throusand;
            System.out.printf("Mệnh giá %s: có %d tờ %n", decimalFormat.format(Two_hundred_throusand), twohundredthrousand);
        }



        if(money >= one_hundred_throusand){
            onehundredthrousand = money / one_hundred_throusand;
            money = money % one_hundred_throusand;
            System.out.printf("Mệnh giá %s: có %d tờ %n", decimalFormat.format(one_hundred_throusand), onehundredthrousand);
        }


        if(money >= Fifty_throusand){
            fiftythrousand = money / Fifty_throusand;
            money = money % Fifty_throusand;
            System.out.printf("Mệnh giá %s: có %d tờ %n", decimalFormat.format(Fifty_throusand), fiftythrousand);
        }


        if(money >= Tweenty_throusand){
            tweentythrousand = money / Tweenty_throusand;
            money = money % Fifty_throusand;
            System.out.printf("Mệnh giá %s: có %d tờ %n", decimalFormat.format(Tweenty_throusand), tweentythrousand);
        }

        if(money >= Ten_throusand){
            tenthrousand = money / Ten_throusand;
            money = money % Ten_throusand;
            System.out.printf("Mệnh giá %s: có %d tờ %n", decimalFormat.format(Ten_throusand), tenthrousand);
        }
    }

}
